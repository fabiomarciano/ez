#include "EZ.h"

int main(int argc, char *argv[]) {

	$(init);

	EZ_NODE *a = NULL;
	a = $.memory.new(sizeof(EZ_NODE) * 1);

	a->value = $.string.new("Fábio Alessandro Marciano");

	EZ_TEXT A = EZ.string.new("FÁBIO Alessandro Marciano New");

	EZ_NODE *token = $.string.split(A, "a");
	EZ_NODE *t = token;

	while (t) {
		printf("%s\n", t->value);
		t = t->next;
	}

	printf("%s\n", $.string.lower(a->value));
	printf("%s\n", $.string.lower(A));

	printf("%s\n", $.string.upper(a->value));
	printf("%s\n", $.string.upper(A));
	printf("%s\n", $.crypt.md5(A));
	printf("%s\n", $.crypt.encode.hex(16));
	printf("%i\n", $.crypt.decode.hex("FF"));
	printf("%s\n", $.crypt.encode.base64("Lorem ipsum dolor sit amet"));

	$.memory.delete(a->value);
	$.memory.delete(A);

/*

	free(EZ.DMAA->address);

	printf("%s\n", a->value);
*/
	$(quit);

	return 0;
}
