/*
 * EZ - Basic C Library
 * -----------------------------------------------------------------------------
 * Licensed under the BSD license, see LICENSE in EZ root for details.
 *
 * mod.node.c - EZ node module file
 *
 * Copyright (c) 2014 Fábio Marciano <fabioamarciano@gmail.com>
 *
 */

#include "mod.node.h"

void __ez_module_node_init__() {
	if (EZ.status && !(EZ.modules & EZ_MODULE_NODE)) {
		EZ.node.new = __ez_module_node_new__;
		EZ.node.length = __ez_module_node_length__;
		EZ.node.rewind = __ez_module_node_rewind__;
		EZ.node.forward = __ez_module_node_forward__;
		EZ.node.index = __ez_module_node_index__;
		EZ.node.jump = __ez_module_node_jump__;
		EZ.node.walk = __ez_module_node_walk__;
		EZ.node.dump = __ez_module_node_dump__;
		EZ.node.push = __ez_module_node_push__;
		EZ.node.unshift = __ez_module_node_unshift__;
		EZ.node.pop = __ez_module_node_pop__;
		EZ.node.shift = __ez_module_node_shift__;
		EZ.node.append = __ez_module_node_append__;
		EZ.node.prepend = __ez_module_node_prepend__;
		EZ.node.detach = __ez_module_node_detach__;
		EZ.node.clear = __ez_module_node_clear__;
		EZ.node.drop = __ez_module_node_drop__;
		EZ.node.set.value = __ez_module_node_set_value__;
		EZ.node.set.attribute = __ez_module_node_set_attribute__;
		EZ.node.get.value = __ez_module_node_get_value__;
		EZ.modules &= EZ_MODULE_NODE;
	}
}

EZ_NODE *__ez_module_node_new__(const char *name, const char *value) {
	EZ_NODE *node = (EZ_NODE *)malloc(sizeof(EZ_NODE));
	node->name = name != NULL ? strdup(name) : NULL;
	node->value = value != NULL ? strdup(value) : NULL;
	node->attributes = NULL;
	node->events = NULL;
	node->childNodes = NULL;
	node->parentNode = NULL;
	node->next = NULL;
	node->previous = NULL;
	return node;
}

unsigned int __ez_module_node_length__(EZ_NODE *node) {
	register unsigned int count = 0;
	EZ_NODE *cursor = NULL;

	if (node != NULL) {
		cursor = node;
		count++;

		while (cursor->previous != NULL) {
			cursor = cursor->previous;
		}

		while (cursor->next != NULL) {
			cursor = cursor->next;
			count++;
		}
	}

	cursor = NULL;

	return count;
}

unsigned int __ez_module_node_rewind__(EZ_NODE *node) {
	register unsigned int count = 0;

	if (node != NULL) {
		while (node->previous != NULL) {
			node = node->previous;
			count++;
		}
	}

	return count;
}

unsigned int __ez_module_node_forward__(EZ_NODE *node) {
	register unsigned int count = 0;

	if (node != NULL) {
		while (node->next != NULL) {
			node = node->next;
			count++;
		}
	}

	return count;
}

unsigned int __ez_module_node_index__(EZ_NODE *node) {
	register int index = 0;
	EZ_NODE *cursor = node;

	if (cursor != NULL) {
		while (cursor->previous != NULL) {
			index++;
			cursor = cursor->previous;
		}
	}

	return index;
}

unsigned int __ez_module_node_jump__(EZ_NODE *node, int position) {
	register int count = 0;
	register int index = position;
	register int length = node != NULL ? __ez_module_node_length__(node) : 0;

	if (node != NULL) {

		__ez_module_node_rewind__(node);

		if (position != 0) {
			index = (index >= length ? index % length : (index < 0 ? (length - ((index * -1) % length)) : index));
			while (index--) {
				node = node->next;
			}
		}
	}

	return count;
}

unsigned int __ez_module_node_walk__(EZ_NODE *node, int steps) {
	register int index = __ez_module_node_index__(node);
	register int length = 0;
	register int cursor = index;
	register int target = 0;

	if (node != NULL) {
		if (steps != 0) {
			length = __ez_module_node_length__(node);
			target = steps >= 0 ? steps % length : length - ((steps * -1) % length);
			target = cursor + target >= length ? target - length : target;

			if (target > 0) {
				while (target--) {
					node = node->next;
				}
			} else {
				target *= -1;
				while (target--) {
					node = node->previous;
				}
			}
		}
	}

	return index;
}

void __ez_module_node_dump__(EZ_NODE *node) {
	EZ_NODE *cursor = node;
	unsigned int tab = 0;
	int i = 0;

	if (cursor != NULL) {
		while (cursor->previous != NULL) {
			cursor = cursor->previous;
		}

		while (cursor) {
			for (i = 0; i < tab; i++) {
				printf(EZ_TAB);
			}
			printf(EZ_DUMP_MODEL, (cursor->name != NULL ? cursor->name : EZ_EMPTY), (cursor->value != NULL ? (char *)cursor->value : EZ_EMPTY));
			if (cursor->childNodes != NULL) {
				cursor = cursor->childNodes;
				tab++;
			} else {
				if (cursor->next != NULL) {
					cursor = cursor->next;
				} else {
					if (cursor->parentNode != NULL) {
						if (cursor->parentNode->next != NULL) {
							cursor = cursor->parentNode->next;
							tab--;
						} else {
							break;
						}
					} else {
						break;
					}
				}
			}
		}
	}
}

void __ez_module_node_push__(EZ_NODE *node, const char *name, const char *value) {
	EZ_NODE *new = __ez_module_node_new__(name != NULL ? name : EZ_EMPTY, value != NULL ? value : EZ_EMPTY);

	if (node != NULL) {

		while (node->next != NULL) {
			node = node->next;
		}
		__ez_module_node_append__(node, new);
	} else {
		node = new;
	}

	__ez_module_node_rewind__(node);
}

void __ez_module_node_unshift__(EZ_NODE *node, const char *name, const char *value) {}

EZ_NODE *__ez_module_node_pop__(EZ_NODE *node) {
	EZ_NODE *output = NULL;
	return output;
}

EZ_NODE *__ez_module_node_shift__(EZ_NODE *node) {
	EZ_NODE *output = NULL;
	return output;
}

void __ez_module_node_append__(EZ_NODE *node, EZ_NODE *child) {

	if (node != NULL) {

		while (child->previous != NULL) {
			child = child->previous;
		}

		while (node->next != NULL) {
			node = node->next;
		}

		node->next = child;
		child->previous = node;
		child->parentNode = node->parentNode;
	} else {
		node = child;
	}

	__ez_module_node_rewind__(node);

}

void __ez_module_node_prepend__(EZ_NODE *node, EZ_NODE *child) {

	if (node != NULL) {

		while (child->next != NULL) {
			child = child->next;
		}

		while (node->previous != NULL) {
			node = child->previous;
		}

		node->previous = child;
		child->next = node;
		child->parentNode = node->parentNode;

	} else {
		node = child;
	}

	__ez_module_node_rewind__(node);
}

void __ez_module_node_detach__(EZ_NODE *node) {

	if (node != NULL) {
		if (node->next != NULL) {
			node->next->previous = node->previous != NULL ? node->previous : NULL;
		}

		if (node->previous != NULL) {
			node->previous->next = node->next != NULL ? node->next : NULL;
		}

		node->next = NULL;
		node->previous = NULL;
		node->parentNode = NULL;
	}
}

void __ez_module_node_clear__(EZ_NODE *node) {
	EZ_NODE *cursor = node;

	printf("LIMPANDO:\n\tName: %s\n\tValue: %s\n\n", cursor->name, (char *)cursor->value);

	// CHECK CHILD EZ_NODES
	// CHECK NEXT
	// CHECK PREVIOUS
	// CLEAR VALUE
	// CLEAR NAME
	// CLEAR ATTRIBUTES
	// CLEAR EVENTS

	free(cursor->name);
	free(cursor->value);
	cursor->name = NULL;
	cursor->value = NULL;

	cursor = NULL;
}

void __ez_module_node_drop__(EZ_NODE *node) {
	__ez_module_node_detach__(node);
	__ez_module_node_clear__(node);
}

void __ez_module_node_set_attribute__(EZ_NODE *node, const char *name, const char *value) {}

void __ez_module_node_set_event__(EZ_NODE *node, const char *name, const char *value) {}

void __ez_module_node_set_value__(EZ_NODE *node, const char *value) {
	if (node != NULL) {
		if (node->value != NULL) {
			free(node->value);
			node->value = NULL;
		}

		if (value != NULL) {
			node->value = strdup(value);
		} else {
			node->value = NULL;
		}
	}
}

void __ez_module_node_add_event__(EZ_NODE *node, const char *name, const char *value) {}

char *__ez_module_node_get_event__(EZ_NODE *node, const char *name) {
	char *output = NULL;
	return output;
}

char *__ez_module_node_get_attribute__(EZ_NODE *node, const char *name) {
	char *output = NULL;
	return output;
}

char *__ez_module_node_get_value__(EZ_NODE *node, const char *name) {
	char *output = NULL;
	EZ_NODE *cursor = node;

	while (cursor->previous != NULL) {
		cursor = cursor->previous;
	}

	while (cursor != NULL) {
		if ((__ez_module_string_compare__(cursor->name, name))) {
			output = cursor->value;
			break;
		}
		cursor = cursor->next;
	}

	return output;
}