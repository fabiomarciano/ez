/*
 * EZ - Basic C Library
 * -----------------------------------------------------------------------------
 * Licensed under the BSD license, see LICENSE in EZ root for details.
 *
 * mod.memory.c - EZ memory module file
 *
 * Copyright (c) 2014 Fábio Marciano <fabioamarciano@gmail.com>
 *
 */

#include "mod.memory.h"

void __ez_module_memory_init__() {
	if (EZ.status && !(EZ.modules & EZ_MODULE_MEMORY)) {
		EZ.memory.new = __ez_module_memory_new__;
		EZ.memory.resize = __ez_module_memory_resize__;
		EZ.memory.delete = __ez_module_memory_delete__;
		EZ.modules &= EZ_MODULE_MEMORY;
	}
}

void *__ez_module_memory_new__(int size) {
	void *block = NULL;
	block = size > 0 ? calloc(1, size) : block;
	return block;
}

void __ez_module_memory_resize__(void *block, int size) {
	block = realloc(block, size);
	if (block == NULL) {
		exit(0x0002);
	}
}

void __ez_module_memory_delete__(void *block) {
	if (block) {
		free(block);
	}
}
