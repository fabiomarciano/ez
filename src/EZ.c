/*
 * EZ - Basic C Library
 * -----------------------------------------------------------------------------
 * Licensed under the BSD license, see LICENSE in EZ root for details.
 *
 * main.c - EZ main file
 *
 * Copyright (c) 2014 Fábio Marciano <fabioamarciano@gmail.com>
 *
 */

#include "EZ.h"

void __ez_init__() {
	if (!EZ.status) {
		EZ.status |= 1;

		setlocale(LC_ALL, EZ_LOCALE);

		__ez_module_memory_init__();
		__ez_module_node_init__();
		__ez_module_string_init__();
		__ez_module_crypt_init__();
		__ez_module_db_init__();
	}
}

void __ez_quit__() {
	if (EZ.status) {
		EZ.status &= 0;
	}
}
