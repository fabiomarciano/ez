/*
 * EZ - Basic C Library
 * -----------------------------------------------------------------------------
 * Licensed under the BSD license, see LICENSE in EZ root for details.
 *
 * mod.db.c - EZ db module file
 *
 * Copyright (c) 2014 Fábio Marciano <fabioamarciano@gmail.com>
 *
 */

#include "mod.db.h"

void __ez_module_db_init__() {
	if (EZ.status && !(EZ.modules & EZ_MODULE_DB)) {

		EZ.db.new = __ez_module_db_new__;
		EZ.db.delete = __ez_module_db_delete__;
		EZ.db.set = __ez_module_db_set__;

		EZ.modules &= EZ_MODULE_DB;
	}
}

EZ_DBO *__ez_module_db_new__(EZ_DB_TYPE driver) {

	EZ_DBO *dbo = __ez_module_memory_new__(sizeof(EZ_DBO));
	dbo->type = driver;
	dbo->status = 0;

	__ez_module_db_set__(dbo, EZ_DB_ATTR_HOST, EZ_DB_HOST);
	__ez_module_db_set__(dbo, EZ_DB_ATTR_USER, EZ_DB_USER);
	__ez_module_db_set__(dbo, EZ_DB_ATTR_PASSWORD, EZ_DB_PASSWORD);
	__ez_module_db_set__(dbo, EZ_DB_ATTR_NAME, EZ_DB_NAME);

	switch (driver) {
		/*
		case EZ_DB_TYPE_POSTGRES:
			db.connection.pgsql = PQconnectdb(EZ_EMPTY);
			EZ_SET_ATTRIBUTE(db, name, "template1");
			EZ_SET_ATTRIBUTE(db, user, EZ_DB_POSTGRES_USER);
		break;*/
		default:
			mysql_init(dbo->connection.mysql);
			__ez_module_db_set__(dbo, EZ_DB_ATTR_PORT, EZ_DB_MYSQL_PORT);
		break;
	}

	return dbo;
}

void __ez_module_db_delete__(EZ_DBO *dbo) {
	if (dbo) {
		switch (dbo->type) {
			/*
			case EZ_DB_TYPE_POSTGRES:
				db.connection.pgsql = PQconnectdb(EZ_EMPTY);
				EZ_SET_ATTRIBUTE(db, name, "template1");
				EZ_SET_ATTRIBUTE(db, user, EZ_DB_POSTGRES_USER);
			break;*/
			default:
				mysql_close(dbo->connection.mysql);
				dbo->connection.mysql = NULL;
			break;
		}

		__ez_module_db_unset__(dbo, EZ_DB_ATTR_HOST);
		__ez_module_db_unset__(dbo, EZ_DB_ATTR_USER);
		__ez_module_db_unset__(dbo, EZ_DB_ATTR_PASSWORD);
		__ez_module_db_unset__(dbo, EZ_DB_ATTR_NAME);

		dbo->type = 0;
	}
}

void __ez_module_db_set__(EZ_DBO *dbo, EZ_DB_ATTR attribute, const char *value) {
	if (dbo) {
		switch (attribute) {
			case EZ_DB_ATTR_HOST:
				EZ_SET_ATTRIBUTE(dbo, host, value);
			break;
			case EZ_DB_ATTR_USER:
				EZ_SET_ATTRIBUTE(dbo, user, value);
			break;
			case EZ_DB_ATTR_PASSWORD:
				EZ_SET_ATTRIBUTE(dbo, password, value);
			break;
			case EZ_DB_ATTR_PORT:
				EZ_SET_ATTRIBUTE(dbo, port, value);
			break;
			default:
				EZ_SET_ATTRIBUTE(dbo, name, value);
			break;
		}
	}
}

void __ez_module_db_unset__(EZ_DBO *dbo, EZ_DB_ATTR attribute) {
	if (dbo) {
		switch (attribute) {
			case EZ_DB_ATTR_HOST:
				EZ_UNSET_ATTRIBUTE(dbo, host);
			break;
			case EZ_DB_ATTR_USER:
				EZ_UNSET_ATTRIBUTE(dbo, user);
			break;
			case EZ_DB_ATTR_PASSWORD:
				EZ_UNSET_ATTRIBUTE(dbo, password);
			break;
			case EZ_DB_ATTR_PORT:
				EZ_UNSET_ATTRIBUTE(dbo, port);
			break;
			default:
				EZ_UNSET_ATTRIBUTE(dbo, name);
			break;
		}
	}
}
