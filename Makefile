#
# FILE		Makefile
# DATE		2015-03-02
# AUTHOR	FABIO MARCIANO
# SUMMARY	Makefile for Chronos project
#

USRDIR = .
INCDIR = $(USRDIR)/include
BINDIR = $(USRDIR)/bin
SRCDIR = $(USRDIR)/src
LIBLBL = EZ
PREFIX = mod
CC = gcc
CFLAGS = -fPIC -Wall -I $(INCDIR) -c -o
DFLAGS = -shared -Wl,-soname,lib$(LIBLBL).so -o $(BINDIR)/lib$(LIBLBL).so
LDFLAGS=
EXECUTABLE = $(LIBLBL)
# SOURCES = $(PREFIX).node.c $(PREFIX).string.c $(PREFIX).crypt.c $(PREFIX).http.c $(PREFIX).db.c $(LIBLBL).c
SOURCES = $(SRCDIR)/$(PREFIX).memory.c $(SRCDIR)/$(PREFIX).node.c $(SRCDIR)/$(PREFIX).string.c $(SRCDIR)/$(PREFIX).crypt.c $(SRCDIR)/$(PREFIX).db.c $(SRCDIR)/$(LIBLBL).c
OBJECTS = $(SOURCES:.c=.o)
PACKAGE = ar rs

all: purge $(OBJECTS) $(EXECUTABLE) clean

$(EXECUTABLE):
	@if ! [ -d $(BINDIR) ] ; then echo Creating $(BINDIR); mkdir $(BINDIR) ; fi;
	@echo Packing $(BINDIR)/$@.a
	@$(PACKAGE) $(BINDIR)/$@.a $(SRCDIR)/$@.o $(SOURCES:.c=.o) > /dev/null 2> /dev/null
	@echo Packing $(BINDIR)/lib$(LIBLBL).so
	@$(CC) $(DFLAGS) $(SOURCES:.c=.o)
	@echo Done.

.c.o:
	@$(CC) $(CFLAGS) $@ $<
	@if [ -f '$@' ] ; then tput setaf 2 && echo -n " [OK]\t" && tput setaf 3 && echo $@; else tput setaf 1 && echo -n " [NO]\t" && tput setaf 3 && echo $@; fi;
	@tput sgr0

purge:
	@rm -f $(BINDIR)/*.a $(BINDIR)/*.so & rm -f $(SRCDIR)/*.o

clean:
	@rm -f $(SRCDIR)/*.o
