/*
 * EZ - Basic C Library
 * -----------------------------------------------------------------------------
 * Licensed under the BSD license, see LICENSE in EZ root for details.
 *
 * main.h - EZ main header file
 *
 * Copyright (c) 2014 Fábio Marciano <fabioamarciano@gmail.com>
 *
 */

#ifndef _EZ_H_
#define _EZ_H_ 1

#include "incs.h"
#include "type.h"
#include "defs.h"
#include "macro.h"
#include "mods.h"

/**
 * Trigger to init or quit EZ library
 */
#define EZ(ARG0) ({\
 	__ez_ ## ARG0 ## __();\
})

/**
 * Start of File
 */
__SOF__

/**
 * EZ Init
 *
 * @param	void
 * @return	void
 */
void __ez_init__(void);

/**
 * EZ Quit
 *
 * @param	void
 * @return	void
 */
void __ez_quit__(void);

/**
 * End of File
 */
__EOF__

#endif
