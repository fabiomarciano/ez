/*
 * EZ - Basic C Library
 * -----------------------------------------------------------------------------
 * Licensed under the BSD license, see LICENSE in EZ root for details.
 *
 * macro.h - EZ generic macros header file
 *
 * Copyright (c) 2014 Fábio Marciano <fabioamarciano@gmail.com>
 *
 */

#ifndef _EZ_MACRO_H_
#define _EZ_MACRO_H_ 1

#define EZ_SET_ATTRIBUTE(ARG0, ARG1, ARG2) ARG0->ARG1 = strdup(ARG2);

#define EZ_UNSET_ATTRIBUTE(ARG0, ARG1) ({\
	if (ARG0->ARG1) {\
		free(ARG0->ARG1);\
		ARG0->ARG1 = NULL;\
	}\
})

#define EZ_DB_GET_DRIVER(ARG0) ({\
	EZ_DB_TYPE __t__ = (EZ_DB_TYPE) ARG0;\
	switch (__t__) {\
		case 1:\
			mysql;\
		break;\
		case 2:\
			pgsql;\
		break;\
		default:\
			mysql\
		break;\
	}\
})

#endif
