/**
 * EZ - Basic C Library
 * -----------------------------------------------------------------------------
 * Licensed under the BSD license, see LICENSE in EZ root for details.
 *
 * mod.db.h - EZ db module file
 *
 * Copyright (c) 2014 Fábio Marciano <fabioamarciano@gmail.com>
 *
 */

#ifndef _EZ_MODS_DB_H_
#define _EZ_MODS_DB_H_ 1

#include "incs.h"
#include "defs.h"
#include "macro.h"
#include "type.h"
#include "mods.h"

__SOF__

/**
 * Database module constructor
 */
extern void __ez_module_db_init__(void);

/**
 * Creates a new database resource.
 * @param	int type of database
 * @return	EZ_DBO dp pointer
 */
extern EZ_DBO *__ez_module_db_new__(EZ_DB_TYPE);

/**
 * Drops an database object
 * @param	EZ_DBO pointer
 */
extern void __ez_module_db_delete__(EZ_DBO *);

/**
 * Setup the DB_DATA attribute/value.
 * @param	EZ_DBO pointer
 * @param	char attribute name
 * @param	char attribute value
 */
extern void __ez_module_db_set__(EZ_DBO *, EZ_DB_ATTR, const char *);

/**
 * Unset the DB_DATA attribute.
 * @param	EZ_DBO pointer
 * @param	char attribute name
 */
extern void __ez_module_db_unset__(EZ_DBO *, EZ_DB_ATTR);

/**
 * Select a new database.
 * @param	EZ_DBO pointer
 * @param	char attribute database name
 */
extern void __ez_module_db_use__(EZ_DBO *, const char *);

__EOF__

#endif
