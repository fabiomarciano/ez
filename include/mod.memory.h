/**
 * EZ - Basic C Library
 * -----------------------------------------------------------------------------
 * Licensed under the BSD license, see LICENSE in EZ root for details.
 *
 * mod.memory.h - EZ memory module file
 *
 * Copyright (c) 2014 Fábio Marciano <fabioamarciano@gmail.com>
 *
 */

#ifndef _EZ_MODS_MEMORY_H_
#define _EZ_MODS_MEMORY_H_ 1

#include "incs.h"
#include "defs.h"
#include "macro.h"
#include "type.h"
#include "mods.h"

__SOF__

/**
 * Memory module constructor
 */
extern void __ez_module_memory_init__(void);

/**
 * Create a new memory block
 *
 * @param	int size
 * @return	void * allocated memory block
 */
extern void *__ez_module_memory_new__(int);

/**
 * Resize a memory block
 *
 * @param	void * block
 * @param	int size
 * @return	void none
 */
extern void __ez_module_memory_resize__(void *, int);

/**
 * Delete a memory block
 *
 * @param	void * block
 * @return	void none
 */
extern void __ez_module_memory_delete__(void *);

__EOF__

#endif
