/**
 * EZ - Basic C Library
 * -----------------------------------------------------------------------------
 * Licensed under the BSD license, see LICENSE in EZ root for details.
 *
 * mod.string.h - EZ string module file
 *
 * Copyright (c) 2014 Fábio Marciano <fabioamarciano@gmail.com>
 *
 */

#ifndef _EZ_MODS_STRING_H_
#define _EZ_MODS_STRING_H_ 1

#include "incs.h"
#include "defs.h"
#include "macro.h"
#include "type.h"
#include "mods.h"

__SOF__

/**
 * Memory module constructor
 */
extern void __ez_module_string_init__(void);

/**
 * Create a new string
 *
 * @param	char * const string
 * @return	char * string
 */
extern char *__ez_module_string_new__(const char *);

/**
 * Counts the number of substrings
 *
 * @param	char * search string
 * @param	char * subject string
 * @return	int number of substring occurrences
 */
extern unsigned int __ez_module_string_substring_count__(const char *, const char *, ...);

/**
 * Replace string
 *
 * @param	char search string
 * @param	char substitutive string
 * @param	char subject string
 * @return	char modified string
 */
extern char *__ez_module_string_replace__(const char *, const char *, const char *, ...);

/**
 * Split string
 *
 * @param	char subject
 * @param	char delimiter
 * @return	EZ_NODE of substrings
 */
extern EZ_NODE *__ez_module_string_split__(const char *, const char *, ...);

/**
 * Join string
 *
 * @param	NODE array of strings
 * @param	char joint
 * @return	char consolidated string
 */
extern char *__ez_module_string_join__(EZ_NODE *, const char *, ...);

/**
 * Trim string
 *
 * @param	char string
 * @param	char character to trim
 * @return	char trimmed string
 */
extern char *__ez_module_string_trim__(const char *, char);

/**
 * Left trim string
 *
 * @param	char string
 * @param	char character to trim
 * @return	char left trimmed string
 */
extern char *__ez_module_string_left_trim__(const char *, char);

/**
 * Right trim string
 *
 * @param	char string
 * @param	char character to trim
 * @return	char right trimmed string
 */
extern char *__ez_module_string_right_trim__(const char *, char);

/**
 * Lowercase string
 *
 * @param	char string
 * @return	char lowercase string
 */
extern char *__ez_module_string_to_lowercase__(const char *);

/**
 * Uppercase string
 *
 * @param	char string
 * @return	char uppercase string
 */
extern char *__ez_module_string_to_uppercase__(const char *);

/**
 * Compare two strings
 *
 * @param	char string #1
 * @param	char string #2
 * @return	int 1 if the strings are equals
 */
extern int __ez_module_string_compare__(const char *, const char *);

/**
 * Converts integer number to string
 *
 * @param	int number
 * @return	char stringfied number
 */
extern char *__ez_module_string_integer_to_string__(int);

/**
 * Querystring parser
 *
 * @param	char subject
 * @return	NODE structure with parsed querystring.

extern NODE *__ez_module_string_parse_querystring__(char *);

 */

__EOF__

#endif
