/*
 * EZ - Basic C Library
 * -----------------------------------------------------------------------------
 * Licensed under the BSD license, see LICENSE in EZ root for details.
 *
 * defs.h - EZ definition header file
 *
 * Copyright (c) 2014 Fábio Marciano <fabioamarciano@gmail.com>
 *
 */

#ifndef _EZ_DEFS_H_
#define _EZ_DEFS_H_ 1

/**
 * Start Of File (__SOF__) and End Of File (__EOF__) definition. Set up to C function definitions even when using C++.
 */
#ifdef __cplusplus
	#define __SOF__ extern "C" {
	#define __EOF__ }
#else
	#define __SOF__
	#define __EOF__
#endif

/**
 * EZ structure alias
 */
#define $ EZ

/**
 * EZ variadic arguments macro
 */
#define args(...) (0, ##__VA_ARGS__, NULL)

// @def	EZ_CHAR_NULL
// Null character.
#define EZ_CHAR_NULL 0x00

// @def	EZ_CHAR_WHITE_SPACE
// No-break space character.
#define EZ_CHAR_WHITE_SPACE 0x20

// @def	EZ_CHAR_UNDERSCORE
// Underscore character.
#define EZ_CHAR_UNDERSCORE 0x5F

// @def	EZ_CHAR_AMPERSAND
// Ampersand character.
#define EZ_CHAR_AMPERSAND 0x26

// @def	EZ_CHAR_PLUS
// Plus sign/operator character.
#define EZ_CHAR_PLUS 0x2B

// @def	EZ_CHAR_PERCENT
// Percent/module sign/operator character.
#define EZ_CHAR_PERCENT 0x25

// @def	EZ_EMPTY
// Empty String.
#define EZ_EMPTY ""

// @def	EZ_CARRIAGE_RETURN
// Carriage return string.
#define EZ_CARRIAGE_RETURN "\r"

// @def	EZ_LINE_FEED
// Line feed string.
#define EZ_LINE_FEED "\n"

// @def	EZ_TAB
// Tab string.
#define EZ_TAB "\t"

// @def	EZ_EQUAL
// Equal string.
#define EZ_EQUAL "="

// @def	EZ_LT
// Less than string.
#define EZ_LT "<"

// @def	EZ_GT
// Greater than string.
#define EZ_GT ">"

// @def	EZ_DOT
// Dot string.
#define EZ_DOT "."

// @def	EZ_UNDERSCORE
// Dot string.
#define EZ_UNDERSCORE "_"

// @def	EZ_QUERYSTRING_DELIMITER
// Querystring delimiter string.
#define EZ_QUERYSTRING_DELIMITER "&"

// @def	EZ_QUERYSTRING_TOKEN_DELIMITER
// Querystring token delimiter string.
#define EZ_QUERYSTRING_TOKEN_DELIMITER EZ_EQUAL

// @def	EZ_DUMP_MODEL
// Dump model string.
#define EZ_DUMP_MODEL "%s " EZ_EQUAL EZ_GT " %s" EZ_LINE_FEED

// @def	EZ_CRLF
// Carriage Return and Line Feed Characters Pair by OS.
#if EZ_OS == 1
	#define EZ_CRLF EZ_CARRIAGE_RETURN EZ_LINE_FEED
#elif EZ_OS == 2
	#define EZ_CRLF EZ_CARRIAGE_RETURN EZ_CARRIAGE_RETURN
#else
	#define EZ_CRLF EZ_LINE_FEED EZ_LINE_FEED
#endif

// @def	EZ_LOWER_HEXSET
// Defines the lowercase hexadecimal set of characters.
#define EZ_LOWER_HEXSET "0123456789abcdef"

// @def	EZ_UPPER_HEXSET
// Defines the uppercase hexadecimal set of characters.
#define EZ_UPPER_HEXSET "0123456789ABCDEF"

// @def	EZ_HEXSET
// Defines the hexadecimal set of characters.
#define EZ_HEXSET EZ_LOWER_HEXSET

// @def	EZ_CRYPT_BYTE_FORMAT
// Defines the byte format for 2 characters hexadecimal.
#define EZ_CRYPT_BYTE_FORMAT "%02x"

// @def	EZ_CRYPT_DATA_MAX_LENGTH
// Defines the maximum length of block to process in crypt algoritms.
#define EZ_CRYPT_DATA_MAX_LENGTH 512

// @def	EZ_CRYPT_SALT
// Defines the salt to crypt methods.
#define EZ_CRYPT_SALT "FabioMarciano"

// @def	EZ_MIME_TEXT_PLAIN
// Defines the mime type text/plain.
#define EZ_MIME_TEXT_PLAIN "text/plain"

// @def	EZ_MIME_TEXT_HTML
// Defines the mime type text/html.
#define EZ_MIME_TEXT_HTML "text/html"

// @def	EZ_MIME_IMAGE_JPEG
// Defines the mime type image/jpeg.
#define EZ_MIME_IMAGE_JPEG "image/jpeg"

// @def	EZ_MIME_IMAGE_PNG
// Defines the mime type image/png.
#define EZ_MIME_IMAGE_PNG "image/png"

// @def	EZ_MIME_IMAGE_BMP
// Defines the mime type image/bmp.
#define EZ_MIME_IMAGE_BMP "image/bmp"

// @def	EZ_MIME_IMAGE_GIF
// Defines the mime type image/gif.
#define EZ_MIME_IMAGE_GIF "image/gif"

// @def	EZ_CHARSET
// Defines the default charset.
#define EZ_CHARSET "UTF-8"

// @def	EZ_LANGUAGE
// Defines the lang.
#define EZ_LANG "pt"

// @def	EZ_COUNTRY_ISO
// Defines the country abbreviation (ISO).
#define EZ_COUNTRY_ISO "BR"

// @def	EZ_COUNTRY_LABEL
// Defines the country label.
#define EZ_COUNTRY_LABEL "Brasil"

// @def	EZ_LANGUAGE
// Defines the language.
#define EZ_LANGUAGE EZ_LANG EZ_UNDERSCORE EZ_COUNTRY_ISO

// @def	EZ_LOCALE
// Localization.
#define EZ_LOCALE EZ_LANGUAGE EZ_DOT EZ_CHARSET

// @def	EZ_HTTP_CHARSET
// Defines default HTTP charset.
#define EZ_HTTP_CHARSET "charset" EZ_EQUAL EZ_CHARSET

// @def	EZ_HTTP_CONTENT_TYPE
// Defines default HTTP content type.
#define EZ_HTTP_CONTENT_TYPE "Content-type:" EZ_MIME_TEXT_HTML

// @def	EZ_HTTP_HEADER
// Defines default HTTP response header.
#define EZ_HTTP_HEADER EZ_HTTP_CONTENT_TYPE ";" EZ_HTTP_CHARSET EZ_LINE_FEED EZ_LINE_FEED

// @def	EZ_HTTP_ENCTYPE_URL_ENCODED
// Defines HTTP ENCTYPE url encoded.
#define EZ_HTTP_ENCTYPE_URL_ENCODED "application/x-www-form-urlencoded"

// @def	EZ_HTTP_ENCTYPE_MULTIPART
// Defines HTTP ENCTYPE multipart.
#define EZ_HTTP_ENCTYPE_MULTIPART "multipart/form-data"

// @def	EZ_HTTP_ENCTYPE
// Defines default HTTP ENCTYPE mode.
#define EZ_HTTP_ENCTYPE EZ_HTTP_ENCTYPE_URL_ENCODED

// @def	EZ_DB_HOST
// Defines default database host.
#define EZ_DB_HOST "localhost"

// @def	EZ_DB_USER
// Defines default database user.
#define EZ_DB_USER "root"

// @def	EZ_DB_PASSWORD
// Defines default database password.
#define EZ_DB_PASSWORD "admin"

// @def	EZ_DB_NAME
// Defines default database name.
#define EZ_DB_NAME ""

// @def	EZ_DB_PORT
// Defines mysql database port.
#define EZ_DB_MYSQL_PORT "3306"

// @def	EZ_DB_POSTGRES_USER
// Defines postgres database user.
#define EZ_DB_POSTGRES_USER "postgres"

// @def	EZ_DB_POSTGRES_PORT
// Defines postgres database port.
#define EZ_DB_POSTGRES_PORT "5432"

#endif
