/**
 * EZ - Basic C Library
 * -----------------------------------------------------------------------------
 * Licensed under the BSD license, see LICENSE in EZ root for details.
 *
 * mod.node.h - EZ node module file
 *
 * Copyright (c) 2014 Fábio Marciano <fabioamarciano@gmail.com>
 *
 */

#ifndef _EZ_MODS_EZ_NODE_H_
#define _EZ_MODS_EZ_NODE_H_ 1

#include "incs.h"
#include "defs.h"
#include "macro.h"
#include "type.h"
#include "mods.h"

__SOF__

/**
 * Memory module constructor
 */
extern void __ez_module_node_init__(void);

/**
 * Creates and return a pointer to a named/valued node.
 *
 * @param	char node name
 * @param	char node value
 * @return	EZ_NODE pointer to new EZ_NODE
 */
extern inline EZ_NODE *__ez_module_node_new__(const char *, const char *);

/**
 * Counts the number of elements of an chainable structure.
 * @param	EZ_NODE pointer to pointer
 * @return	unsigned int total of elements
 */
extern inline unsigned int __ez_module_node_length__(EZ_NODE *);

/**
 * Rewinds the pointer cursor to first position.
 * @param	EZ_NODE pointer to pointer
 * @return	unsigned int the number of steps back
 */
extern inline unsigned int __ez_module_node_rewind__(EZ_NODE *);

/**
 * Forwards the pointer cursor to last position.
 * @param	void pointer to pointer
 * @return	unsigned int the number of steps forward
 */
extern inline unsigned int __ez_module_node_forward__(EZ_NODE *);

/**
 * Gets the actual cursor's position at the chainable structure.
 * @param	EZ_NODE pointer to chainable structure
 * @return	unsigned int actual cursor's position at the chainable structure
 */
extern inline unsigned int __ez_module_node_index__(EZ_NODE *);

/**
 * Jumps the cursor pointer to the passed position.
 * @param	EZ_NODE pointer to pointer
 * @param	int position
 * @return	unsigned int the number of final position
 */
extern inline unsigned int __ez_module_node_jump__(EZ_NODE *, int);

/**
 * Move the cursor pointer "n" positions forward or backward.
 * @param	EZ_NODE pointer to pointer
 * @param	int offset
 * @return	unsigned int the number of final position
 */
extern inline unsigned int __ez_module_node_walk__(EZ_NODE *, int);

/**
 * Prints all name/value pairs in chainable structure.
 * @param	EZ_NODE pointer to chainable structure
 * @return	void
 */
extern inline void __ez_module_node_dump__(EZ_NODE *);

/**
 * Inserts a new node at the end of chainable structure.
 * @param	void pointer to chainable structure
 * @return	void
 */
extern inline void __ez_module_node_push__(EZ_NODE *, const char *, const char *);

/**
 * Inserts a new node at the start of chainable structure.
Attention: this operation resets the node's cursor.
 * @param	void pointer to chainable structure
 * @return	void
 */
extern inline void __ez_module_node_unshift__(EZ_NODE *, const char *, const char *);

/**
 * Removes the last node of the chainable structure.
 * @param	void pointer to chainable structure
 * @return	EZ_NODE pointer to removed node
 */
extern inline EZ_NODE *__ez_module_node_pop__(EZ_NODE *);

/**
 * Removes the first node of the chainable structure.
 * Attention: this operation resets the node's cursor.
 * @param	void pointer to chainable structure
 * @return	EZ_NODE pointer to removed node
 */
extern inline EZ_NODE *__ez_module_node_shift__(EZ_NODE *);

/**
 * Inserts an existed node at the end of chainable structure.
 * Attention: this operation resets the node's cursor.
 * @param	void pointer to chainable structure
 * @param	void pointer to chainable structure
 * @return	void
 */
extern inline void __ez_module_node_append__(EZ_NODE *, EZ_NODE *);

/**
 * Inserts an existed node at the start of chainable structure.
 * Attention: this operation resets the node's cursor.
 * @param	void pointer to chainable structure
 * @param	void pointer to chainable structure
 * @return	void
 */
extern inline void __ez_module_node_prepend__(EZ_NODE *, EZ_NODE *);

/**
 * Detach a node from his structure.
 * @param	EZ_NODE pointer to chainable structure
 * @return	void
 */
extern inline void __ez_module_node_detach__(EZ_NODE *);

/**
 * Clean up the node and his hierarchy freeling the memory alocated to him.
 * @param	void pointer to chainable structure
 * @return	void
 */
extern inline void __ez_module_node_clear__(EZ_NODE *);

/**
 * Detach and clean up the node.
 * @param	EZ_NODE pointer to chainable structure
 * @return	EZ_NODE pointer to EZ_NODE parentNode
 */
extern inline void __ez_module_node_drop__(EZ_NODE *);

/**
 * Sets a new node value
 * @param	EZ_NODE pointer to chainable structure
 * @param	char new value to set
 * @return	void
 */
extern inline void __ez_module_node_set_value__(EZ_NODE *, const char *);

/**
 * Sets a new node value
 * @param	EZ_NODE pointer to chainable structure
 * @param	char attribute name
 * @param	char attribute value
 * @return	void
 */
extern inline void __ez_module_node_set_attribute__(EZ_NODE *, const char *, const char *);

/**
 * Returns the value by name
 * @param	EZ_NODE pointer to chainable structure
 * @param	char attribute name
 * @return	char value stored
 */
extern inline char *__ez_module_node_get_value__(EZ_NODE *, const char *);

__EOF__

#endif
