/**
 * EZ - Basic C Library
 * -----------------------------------------------------------------------------
 * Licensed under the BSD license, see LICENSE in EZ root for details.
 *
 * mod.crypt.h - EZ crypt module file
 *
 * Copyright (c) 2014 Fábio Marciano <fabioamarciano@gmail.com>
 *
 */

#ifndef _EZ_MODS_CRYPT_H_
#define _EZ_MODS_CRYPT_H_ 1

#include "incs.h"
#include "defs.h"
#include "macro.h"
#include "type.h"
#include "mods.h"

__SOF__

/**
 * Memory module constructor
 */
extern void __ez_module_crypt_init__(void);

/**
 * String encryptation basead on MD5 hash algorithm.
 * @param	const char string
 * @return	char 32 bytes MD5 hash
 */
extern char *__ez_module_crypt_md5__(const char *, ...);

/**
 * String encryptation basead on SHA-1 hash algorithm.
 * @param	const char string
 * @return	char 40 bytes SHA-1 hash
 */
extern char *__ez_module_crypt_sha1__(const char *, ...);

/**
 * String encode basead on Base 64 algorithm.
 * @param	const char string
 * @return	char Base 64 encoded string
 */
extern char *__ez_module_crypt_encode_base64__(const char *, ...);

/**
 * String decode basead on Base 64 algorithm.
 * @param	const char base 64 encoded string
 * @return	char base 64 decoded string
 */
extern char *__ez_module_crypt_decode_base64__(const char *, ...);

/**
 * @param	char string to encode
 * @return	char url encoded string.
 */
extern char *__ez_module_crypt_encode_url__(const char *, ...);

/**
 * @param	char string to decode
 * @return	char url decoded string.
 */
extern char *__ez_module_crypt_decode_url__(const char *, ...);

/**
 * @param	char string to decode
 * @return	int number.
 */
extern int __ez_module_crypt_decode_hex__(const char *, ...);

/**
 * @param	int number to encode
 * @return	char hexadecimal string.
 */
extern char *__ez_module_crypt_encode_hex__(unsigned int, ...);

__EOF__

#endif
