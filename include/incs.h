/*
 * EZ - Basic C Library
 * -----------------------------------------------------------------------------
 * Licensed under the BSD license, see LICENSE in EZ root for details.
 *
 * incs.h - EZ include header file
 *
 * Copyright (c) 2014 Fábio Marciano <fabioamarciano@gmail.com>
 *
 */

#ifndef _EZ_INCS_H_
#define _EZ_INCS_H_ 1

/**
 * ANSI-compatible file and TTY input/output routines.
 */
#include <stdio.h>

/**
 * Date and time functions.
 */
#include <time.h>

/**
 * Routines for checking and changing character types.
 */
#include <ctype.h>

/**
 * Math functions.
 */
#include <math.h>

/**
 * Some useful routines proposed by ANSI (sorting, searching, etc.).
 */
#include <stdlib.h>

/**
 * ANSI routines for creating functions with variable numbers of arguments.
 */
#include <stdarg.h>

/**
 * ANSI definitions of default macros and types.
 */
#include <stddef.h>

/**
 * ANSI-compatible string manipulation routines.
 */
#include <string.h>

/**
 * Internationalization.
 */
#include <locale.h>

/**
 * Wide character types.
 */
#include <wctype.h>

/**
 * Wide character string manipulation routines.
 */
#include <wchar.h>

/**
 * Assert routine for debugging purposes.
 */
#include <assert.h>

/**
 * Defines data types used in system source code.
 */
#include <sys/types.h>

/**
 * POSIX library for the C programming.
 */
#include <sys/stat.h>

/**
 * James Clark's Expat XML parser library.
 */
#include <expat.h>

/**
 * Graphics library for image manipulation.
 */
#include <gd.h>

/**
 * OpenSSL MD5 functions.
 */
#include <openssl/md5.h>

/**
 * OpenSSL SHA functions.
 */
#include <openssl/sha.h>

/**
 * OpenSSL BIO functions.
 */
#include <openssl/bio.h>

/**
 * OpenSSL EVP functions.
 */
#include <openssl/evp.h>

/**
 * MySQL database library.
 */
#include <mysql/mysql.h>

/**
 * MySQL database library error messages.
 */
#include <mysql/errmsg.h>

#endif
