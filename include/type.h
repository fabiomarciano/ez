/*
 * EZ - Basic C Library
 * -----------------------------------------------------------------------------
 * Licensed under the BSD license, see LICENSE in EZ root for details.
 *
 * type.h - EZ type header file
 *
 * Copyright (c) 2014 Fábio Marciano <fabioamarciano@gmail.com>
 *
 */

#ifndef _EZ_TYPE_H_
#define _EZ_TYPE_H_ 1

/**
 * EZ module identifiers
 */

typedef enum {EZ_MODULE_MEMORY = 1, EZ_MODULE_NODE = 1 << 1, EZ_MODULE_STRING = 1 << 2, EZ_MODULE_CRYPT = 1 << 3, EZ_MODULE_DB = 1 << 4} EZ_MODS;

/**
 * EZ text type
 */

typedef char * EZ_TEXT;

/**
 * Node structure.
 */
struct __ez_node__ {
	char *name;
	char *value;
	struct __ez_node__ *attributes;
	struct __ez_node__ *events;
	struct __ez_node__ *childNodes;
	struct __ez_node__ *parentNode;
	struct __ez_node__ *next;
	struct __ez_node__ *previous;
};

typedef struct __ez_node__ EZ_NODE;

/**
 * EZ database type
 */
typedef enum {EZ_DB_TYPE_MYSQL = 1, EZ_DB_TYPE_POSTGRES = 2, EZ_DB_TYPE_DEFAULT = EZ_DB_TYPE_MYSQL} EZ_DB_TYPE;

/**
 * EZ database attribute
 */
typedef enum {EZ_DB_ATTR_TYPE, EZ_DB_ATTR_HOST, EZ_DB_ATTR_USER, EZ_DB_ATTR_PASSWORD, EZ_DB_ATTR_PORT, EZ_DB_ATTR_NAME} EZ_DB_ATTR;

/**
 * EZ database structure
 */
struct __ez_dbo__ {
	int status;
	EZ_DB_TYPE type;
	union {
		/**
		 * MySQL connector
		 */
		MYSQL *mysql;
		/**
		 * Postgres SQL connector
		 */
		// PGconn *pgsql;
	} connection;
	char *host;
	char *user;
	char *password;
	char *name;
	char *port;
};

typedef struct __ez_dbo__ EZ_DBO;

/**
 * EZ module structure
 */
struct {

	/**
	 * Modules
	 */
	unsigned int modules;

	/**
	 * Status of Library
	 */
	int status;

	/**
	 * Holds the environment object structure.
	 */
	EZ_NODE *env;

	/**
	 * Memory module
	 */
	struct {
		void *(*new)(int);
		void (*resize)(void *, int);
		void (*delete)(void *);
	} memory;

	/**
	 * Node module
	 */
	struct {
		EZ_NODE *(*new)(const char *, const char *);
		unsigned int (*length)(EZ_NODE *);
		unsigned int (*rewind)(EZ_NODE *);
		unsigned int (*forward)(EZ_NODE *);
		unsigned int (*index)(EZ_NODE *);
		unsigned int (*jump)(EZ_NODE *, int);
		unsigned int (*walk)(EZ_NODE *, int);
		void (*dump)(EZ_NODE *);
		void (*push)(EZ_NODE *, const char *, const char *);
		void (*unshift)(EZ_NODE *, const char *, const char *);
		EZ_NODE *(*pop)(EZ_NODE *);
		EZ_NODE *(*shift)(EZ_NODE *);
		void (*append)(EZ_NODE *, EZ_NODE *);
		void (*prepend)(EZ_NODE *, EZ_NODE *);
		void (*detach)(EZ_NODE *);
		void (*clear)(EZ_NODE *);
		void (*drop)(EZ_NODE *);
		struct {
			void (*value)(EZ_NODE *, const char *);
			void (*attribute)(EZ_NODE *, const char *, const char *);
		} set;
		struct {
			char *(*value)(EZ_NODE *, const char *);
		} get;
	} node;

	/**
	 * String module
	 */
	struct {
		char *(*new)(const char*);
		unsigned int (*substring_count)(const char *, const char *, ...);
		char *(*replace)(const char *, const char *, const char *, ...);
		EZ_NODE *(*split)(const char *, const char *, ...);
		char *(*join)(EZ_NODE *, const char *, ...);
		char *(*trim)(const char *, char);
		char *(*ltrim)(const char *, char);
		char *(*rtrim)(const char *, char);
		char *(*lower)(const char *);
		char *(*upper)(const char *);
		int (*compare)(const char *, const char *);
		char *(*itostr)(int);
	} string;

	/**
	 * Crypt module
	 */
	struct {
		char *(*md5)(const char *, ...);
		char *(*sha1)(const char *, ...);
		struct {
			char *(*base64)(const char *, ...);
			char *(*url)(const char *, ...);
			int (*hex)(const char *, ...);
		} decode;
		struct {
			char *(*base64)(const char *, ...);
			char *(*url)(const char *, ...);
			char *(*hex)(unsigned int, ...);
		} encode;
	} crypt;

	/**
	 * Database module
	 */
	struct {
		EZ_DBO *(*new)(EZ_DB_TYPE);
		void (*delete)(EZ_DBO *);
		void (*set)(EZ_DBO *, EZ_DB_ATTR, const char *);
	} db;

} EZ;

#endif